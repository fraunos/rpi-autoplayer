const {
    spawn,
    spawnSync
} = require("child_process");
const {
    readFileSync,
    writeFileSync,
    appendFileSync,
    mkdirSync,
    unlinkSync,
    readdirSync,
    statSync
} = require("fs");
const {
    get
} = require("https");

const VERSION = "1.0.5";


const HOME_DIR = '/home/pi/';
const FILES_DIR = `${HOME_DIR}autoplayer-files/`;
const CONFIGS_DIR = `${HOME_DIR}autoplayer-configs/`;
const LOGS_DIR = `${HOME_DIR}autoplayer-logs/`;

const VIDEO_PLAYER = "omxplayer.bin";
const VIDEO_PLAYER_PARAMS = ["--loop", "--no-osd", "--aspect-mode", "stretch"];
const CONFIG_URL = process.argv[2] || "https://www.learningpfdc.pl/config.json";
const filenameRegex = /.*\/(.*\..*)$/;

const DEBUG = false;
const START_TIME = '9:00';
const STOP_TIME = '9:20';

log(`RPI-autoplayer ${VERSION}`);
let programStart = new Date()

function uptimeMinutes() {
    return (new Date() - programStart)/1000/60
}

if (require("os").arch() !== "arm") {
    log("This runs only on raspberry pi!!!");
    process.exit();
}

(async () => {
    log(process.argv);
    
    {
        log('Display Check');
        setDisplayPower(0);
        await sleep(1);
        setDisplayPower(1);
        await sleep(15);
        setDisplayPower(0);
    }

    let config, videoUrl, videoPath, playerPid;

    while (true) {
        let now = Date.now();
        
        if (!config || !videoPath) {
            log("No config set or no videoPath, trying to connect, and download new config")
            try {
                enableModem();
                config = await getConfig(CONFIG_URL)
                log(config)
                videoPath = await downloadVideo(config.videoUrl);
                log(videoPath)
                videoUrl = config.videoUrl;
                videoUrl
            } catch (e) {
                log(e)
            }
        }
        
        try {
            log("Checking new config");
            let newConfig = await getConfig(CONFIG_URL);
            if(newConfig && JSON.stringify(config) !== JSON.stringify(newConfig)) {
                log("Config changed")
                config = newConfig;
                
                if (videoUrl !== config.videoUrl) {
                    log("Video changed")
                    videoPath = await downloadVideo(config.videoUrl);
                    log(videoPath)
                    
                    videoUrl = config.videoUrl;
                }
            }
        } catch (e) {
            log(e)
        }

        log(config);
        log(playerPid);
        log(now);

        if (uptimeMinutes() > 5 && config.startDate - now > 1 * 1000 * 60 && config.startDate - now < 5 * 1000 * 60) {
            spawn("reboot")
        }

        if (videoPath && config && now > config.startDate && now < config.stopDate && !playerPid) {
            log("Video on");
            log(videoPath)
            let {
                pid,
                stdout,
                stderr
            } = spawn(VIDEO_PLAYER, [
                videoPath,
                ...VIDEO_PLAYER_PARAMS
            ]);

            setDisplayPower(1);
            playerPid = pid;
        }
        if (config && now > config.stopDate && playerPid) {
            log("Video off");
            spawn("kill", [playerPid]);
            playerPid = 0;
            setDisplayPower(0);
        }
        await sleep(30);
    }
})();

async function getConfig(url) {
    let config
    try {
        mkdirSync(`${CONFIGS_DIR}`)
    } catch (e){}
    try {
        config = await request(url);
    } catch (e) {
        log(e)
    }
    
    let now = new Date()
    if (config) {
        writeFileSync(`${CONFIGS_DIR}/${getDate()}.config`, JSON.stringify(config))
    } else {
        let configFile = readFileSync(getNewestFileFromDir(CONFIGS_DIR))
        if (configFile) {
            config = JSON.parse(configFile);
        }
    }
       
    return {...config, ...getStartStopDate(config)}
}

function getStartStopDate(config) {
    if (config) {
        const timeRegex = /(\d?\d):(\d\d)/;
        if(DEBUG){
            configT_TIME
            config.stopTime = STOP_TIME
        }

        let startTime = config.startTime.match(timeRegex);
        let startDate = new Date().setHours(
            parseInt(startTime[1], 10),
            parseInt(startTime[2], 10),
            0,
            0
        );
        let stopTime = config.stopTime.match(timeRegex);
        let stopDate = new Date().setHours(
            parseInt(stopTime[1], 10),
            parseInt(stopTime[2], 10),
            0,
            0
        );
        return {
            startDate,
            stopDate
        };
    } else {
        return false;
    }
}

function setDisplayPower(bool) {
    let errCount = 0;
    let err;
    try {
        let write = writeFileSync(
            "/sys/class/backlight/rpi_backlight/bl_power",
            bool ? 0 : 1
        );
    } catch (e) {
        err = e
        errCount++;
    }
    try {
        let {
            stdout,
            stderr
        } = spawnSync("vcgencmd", ["display_power", bool ? 1 : 0]);
    } catch (e) {
        err = e
        errCount++;
    }
    if (errCount === 2) {
        log(e)
    } else {
        log(`Screen ${bool ? "on" : "off"}`);
    }
}

function request(url) {
    return new Promise((resolve, reject) => {
        get(url, res => {
            let data = "";
            res.on("data", d => {
                log("Receiving config data")
                data += d.toString();
            });
            res.on("close", () => {
                log("Config data downloaded")
                log(data);
                resolve(JSON.parse(data));
            });
        }).on('error', (e) => {
            log(e);
            reject()
        });
    });
}

async function downloadVideo(url = '') {
    log("Starting downloading video");
    
    try {
        mkdirSync(`${FILES_DIR}`)
    } catch (e) {
        console.log(e);
    }
    
    let filename = url.match(filenameRegex)[1]

    if (!getAvailableVideos().includes(filename)) {
        try {
            unlinkSync(`${FILES_DIR}/${filename}`)
        } catch (e) {}

        return new Promise((resolve, reject) => {
            get(url, res => {
                res.on("data", d => {
                    appendFileSync(`${FILES_DIR}/${filename}`, d)
                    log("Downloading video");
                });
                res.on("close", () => {
                    log("Video downloaded");
                    resolve(`${FILES_DIR}${filename}`);
                });
            }).on('error', (e) => {
                log(e);
                reject()
            });
        });
    } else {
        log(`Video already on disk, not downloading`);
        return `${FILES_DIR}${filename}`
    }
}

function getNewestFileFromDir(dir) {
    let files = readdirSync(dir)
    console.log(files);
    files = files.map(f => ({
        name: f,
        mtime: statSync(`${dir}/${f}`).mtime
    }))
    console.log(files);
    let filename = files.reduce((p, c) => {
        console.log(p, c)
        return p.mtime < c.mtime ? c : p
    }, files[0]).name
    log(filename);
    return `${dir}${filename}`;
}

function getAvailableVideos() {
    let files = readdirSync(`${FILES_DIR}`)
    log(files);
    return files
}

function checkDiskSpace() {
    
}

function removeOldestVideo() {
    
}

function enableModem() {
    log("Enabling modem");
    //TODO: check usb devices
    let {
        stdout,
        stderr
    } = spawnSync(
        `${HOME_DIR}rpi-autoplayer/deps/sakis3gz`,
        ["connect"], {
            encoding: "utf8"
        }
    );
    log(stderr || stdout);
}

function sleep(seconds) {
    log(`Sleeping for ${seconds} seconds`)
    return new Promise((resolve, reject) => {
        setTimeout(resolve, seconds * 1000);
    });
}

function log(msg) {
    try {
        mkdirSync(`${LOGS_DIR}`)
        console.log(`Created directory ${LOGS_DIR}`);
    } catch (e){}
    
    let now = new Date();
    
    console.log(now.toISOString());
    console.log(msg);
    console.log();
    
    appendFileSync(
        `/home/pi/autoplayer-logs/${getDate()}.logs`,
        `${now.toISOString()}\n${JSON.stringify(msg, null, 4)}\n\n`
    );
    return msg;
}

function getDate() {
    let now = new Date();
    return `${now.getFullYear()}${now.getMonth()}${now.getDate()}`
}
