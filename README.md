# RPI autoplayer

Skrypt do **NodeJS**, dedykowany dla **Raspberry Pi 3B+**, który uruchamiany razem z systemem uruchamia modem 3g (E173, aktualnie dla sieci Play), pobiera plik wideo i odtwarza go w pętli w wyznaczonych w pliku konfiguracyjnym godzinach.

## Instalacja skryptu

```sh
sudo apt-get install nodejs net-tools ppp
git clone https://gitlab.com/fraunos/rpi-autoplayer/
sudo cp ./rpi-autoplayer/deps/sakis3g.conf /etc/
echo "@sudo /usr/bin/nodejs /home/pi/rpi-autoplayer/rpi-autoplayer.js https://fraunos.gitlab.io/rpi-autoplayer/example_config.json" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart # tutaj można podmienić adres pliku konfiguracyjnego
```

## Konfiguracja

[Przykładowy config](https://fraunos.gitlab.io/rpi-autoplayer/example_config.json)

Plik konfiguracyjny jest w formacie **JSON**. Można podać własny config jako pierwszym parametr uruchomieniowy.

Folder `public` automatycznie się publikuje i można przy jego pomocy hostować pliki konfiguracyjne. Będą znajdować się pod adresem `https://fraunos.gitlab.io/rpi-autoplayer/nazwa_pliku_konfiguracyjnego.json`.

### Dostępne pola:

#### `videoUrl`

Adres wideo do pobrania - powinien być z protokołem **https**, skrypt nie wspiera protokołu http.

Raspberry PI ma sprzętową akcelerację wideo zakodowanego przy pomocy h264 (.mp4, .mkv) i nie powinno być problemóœ z takimi materiałami.

#### `startTime`

Godzina (w ustawionej w systemie strefie czasowej) w formacie `HH:MM` lub `H:MM` o której zostanie uruchomione odtwarzanie wideo w pętli.
Wygaszony wyświetlacz zacznie działać.

#### `endTime`

Godzina w formacie takim jak `startTime` o której zostanie zatrzymane odtwarzanie wideo. Ekran zostanie wygaszony.

## Dokładne działanie

Po instalacji skryptu i zrebootowaniu urządzenia dzieją się następujące rzeczy:

1. Pojawia się pulpit środowiska graficznego LXDE, a z nim uruchamiany jest skrypt
2. Skrypt sprawdza zdolność wygaszania ekranu
3. Uruchamiane jest połączenie z internetem przez modem 3g
4. Pobierany jest plik konfiguracyjny
5. Pobierane jest wideo z pliku konfiguracyjnego
6. Uruchamiana jest pętla programu.
    * jeśli aktualna godzina systemu **jest** między `startTime` a `endTime` to uruchamiane jest podświetlenie ekranu i odtwarzanie wideo w pętli
    * jeśli aktualna godzina systemu **nie jest** wtedy ekran pozostaje wygaszony lub jeśli aktualnie było uruchomione wideo, to jest wyłączane i ekran jest wygaszany